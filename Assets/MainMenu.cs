﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class MainMenu : MonoBehaviour {

	ScottGarland.BigInteger nNumber;
	string myName="";
	string myVersion="2c";
	string DirectoryFolder = "/Pictures~/";
	public static string Extention = ".jpg";

	List<Color32> Colors= new List<Color32>();
	string myMessage="Hello World!";

	bool debug=true;
	int iN = 3;
	bool save = true;

	int[,] BitData;
	Texture2D Picture;

	ScottGarland.BigInteger startProgress=new ScottGarland.BigInteger();
	ScottGarland.BigInteger myProgress=new ScottGarland.BigInteger();
	ScottGarland.BigInteger haltProgress=new ScottGarland.BigInteger(100);

	float StartTime = 0;

	bool amIDone()
	{
		return myProgress>(haltProgress+startProgress);
	}

	public bool isDrawing=false;

	int PictureX=10;
	int PictureY=10;

	int DisplayX=5;
	int DisplayY=5;

	void Start () {
		myName = GiveMeName ();
		DirectoryFolder = Application.dataPath + DirectoryFolder;
		Colors = new List<Color32>{ Color.white, Color.black };
		ChangeMessage (0);
		startProgress = checkProgress ();
		haltProgress =new ScottGarland.BigInteger(100);
		nNumber = new ScottGarland.BigInteger (0);	
		iN = 3;
	}

	void Update () {
		if (isDrawing) {
			if (!amIDone ()) {
				for (int I = 0; I < iN; I++) {
					if (translate (myProgress)) {
						UpdateTheImage ();

						myProgress = myProgress + 1;
					}
				}
			} else {
				Stop ();
			}
		}
	}

	void Stop()
	{
		isDrawing = false;
		ChangeMessage (1);
		startProgress=myProgress;
	}

	void Draw()
	{
		StartTime = Time.time;
		if ( !Directory.Exists(DirectoryFolder) ) {
			Directory.CreateDirectory( DirectoryFolder);
		}	
		if ( !Directory.Exists(DirectoryFolder+"/"+myName +myVersion + "_" + PictureX + "x" + PictureY) ) {
			Directory.CreateDirectory( DirectoryFolder+"/"+myName +myVersion + "_" + PictureX + "x" + PictureY);
		}	

		myVersion = Colors.Count + "c";


		myProgress=startProgress;

		ChangeMessage(0);					
		DisplayY = 300;
		DisplayX = DisplayY*PictureX/PictureY;

		isDrawing = true;	
	}

	ScottGarland.BigInteger checkProgress()
	{
		ScottGarland.BigInteger val = new ScottGarland.BigInteger(0);
		while(fileExists(GetImageName(val))){ 
			val=val+1;

		}
		return val;
	}
	void Log()
	{
		if (debug){Debug.Log (myMessage);
		}
	}


	void ChangeMessage(int value)
	{
		if (value==0) {
			myMessage = "Hello! My name is "+myName+" "+myVersion+". Let's draw!";
			Log ();
		}
		else if (value==1)
		{
			myMessage = "I am done for today... It took me "+(Time.time-StartTime)+" seconds!";
			Log ();
		}
		else if (value==2)
		{
			myMessage = "I've drawn everything!!! It took me "+(Time.time-StartTime)+" seconds!";
			Log ();
		}
		else if (value==3)
		{
			myMessage = "Drawing Picture ";
		}
	}

	void OnGUI ()
	{
		float Modifier = Screen.height/900f;

		Rect UIPos = new Rect (Screen.width / 2 - 300*Modifier, Screen.height / 2 - 400*Modifier, 600*Modifier, 800*Modifier);
		Rect RandBtn = new Rect (UIPos.xMin,UIPos.yMin+UIPos.height*8/10f,UIPos.width,UIPos.height/10f);
		Rect StartBtn = new Rect (UIPos.xMin,UIPos.yMin+UIPos.height*9/10f,UIPos.width,UIPos.height/10f);
		Rect SpeedBtn = new Rect (UIPos.xMin,UIPos.yMin+UIPos.height*7/10f,UIPos.width,UIPos.height/10f);
		Rect NamePos = new Rect (UIPos.xMin,UIPos.yMin,UIPos.width,UIPos.height/20f);
		GUI.Box (UIPos, "");
		GUI.Label (NamePos, myMessage);
		if (!isDrawing) {
			Rect PosX = new Rect (UIPos.center.x - 225 * Modifier, UIPos.yMin + 150 * Modifier, 200 * Modifier, 120 * Modifier);
			Rect PosY = new Rect (UIPos.center.x + 25 * Modifier, UIPos.yMin + 150 * Modifier, 200 * Modifier, 120 * Modifier);

			Rect Start = new Rect (UIPos.center.x - 225 * Modifier, UIPos.yMin + 350 * Modifier, 200 * Modifier, 120 * Modifier);
			Rect Stop = new Rect (UIPos.center.x + 25 * Modifier, UIPos.yMin + 350 * Modifier, 200 * Modifier, 120 * Modifier);

			int pPictX = PictureX;
			int pPictY = PictureY;

			PictureX = int.Parse (GUI.TextField (PosX, "" + Mathf.Max(1,PictureX)));
			PictureY = int.Parse (GUI.TextField (PosY, "" + Mathf.Max(1,PictureY)));

			if (pPictX != PictureX || pPictY != PictureY) {

				startProgress = checkProgress ();

				haltProgress = new  ScottGarland.BigInteger(1);}

			startProgress = new ScottGarland.BigInteger(GUI.TextField (Start, startProgress.ToString()));
			haltProgress = new  ScottGarland.BigInteger (GUI.TextField (Stop, "" + haltProgress.ToString()));

			iN = int.Parse (GUI.TextField (SpeedBtn, "" + iN.ToString()));

			/*if (haltProgress == 0) {
				haltProgress = Mathf.Max (1, GetLimit ());
			}*/

			if (GUI.Button (StartBtn, "Start")) {
				Draw();
			}

			if (GUI.Button (RandBtn, "Random ("+startProgress.ToString().Length+")")) {
				startProgress = new ScottGarland.BigInteger((int)(UnityEngine.Random.value*10000))*GetLimit()/10000;
			}
		} else {

			Rect Center = new Rect (UIPos.center.x -UIPos.width/2f, UIPos.center.y -UIPos.width/2f, UIPos.width,UIPos.width);

			GUI.DrawTexture (Center, Picture);

			/*if (isDrawing) {
				if (GUI.Button (StartBtn, nNumber.ToString())) {
					Stop ();
				}
			}
				else */{
				if (GUI.Button (StartBtn, "Stop")) {
					Stop ();
				}
			}
		}
	}

	void Finish()
	{
		myProgress=  haltProgress + startProgress  ;
	}

	string GiveMeName()
	{
		return "Xyrxa";
		/*int Length = Mathf.RoundToInt((6+UnityEngine.Random.value*15)/2);
		string[] Letters = new string[] {
			"A",
			"B",
			"C",
			"D",
			"E",
			"F",
			"G",
			"H",
			"I",
			"J",
			"K",
			"L",
			"M",
			"N",
			"O",
			"P",
			"Q",
			"R",
			"S",
			"T",
			"U",
			"V",
			"W",
			"X",
			"Y",
			"Z"
		};


		List<int> nData = new List<int>();
		for (var Zim=0; Zim<Length; Zim++)
		{
			nData.Add(0);
		}

		var nNumber=UnityEngine.Random.value*60000;
		while (nNumber>0)
		{
			nData[0]+=1;
			nNumber-=1;

			for (var Zim=0; Zim<nData.Count; Zim++)
			{
				if (nData[Zim]>=Letters.Length-1)
				{
					if (nData.Count>Zim) 
					{
						nNumber=0;}
					else
					{
						nData[Zim]-=Colors.Count;
						nData[Zim+1]+=1; }
				}
			}
		}
		string finalName = "";

		foreach (int Gir in nData)
		{
			finalName+=(Letters[Gir%Letters.Length]);
		}
		return finalName;*/

	}
	bool fileExists(string name)
	{
		return File.Exists (DirectoryFolder + name+Extention);
	}

	string GetImageName(ScottGarland.BigInteger progress)
	{
		return myName +myVersion + "_" + PictureX + "x" + PictureY + "/"  + progress.ToString();
	}

	bool translate(ScottGarland.BigInteger myNumber)
	{
		/*if (nNumber.IsZero) {
			BitData.Clear ();
			for (var Zim = 0; Zim < PictureX * PictureY; Zim++) {
				BitData.Add (0);			
			}

			nNumber = new ScottGarland.BigInteger (myProgress.ToString ());
		}

		int Gir=iN; 
		while (!nNumber.IsNegative && !nNumber.IsZero && Gir>0)
		{
			Gir--;
			BitData[0]+=1;
			nNumber=nNumber-1;

			for (var Zim=0; Zim<BitData.Count; Zim++)
			{
				if (BitData[Zim]>Colors.Count-1)
				{
					if (Zim>=BitData.Count && !nNumber.IsZero) 
					{
						BitData.Clear();
						Stop ();
						return true;
					}
					else
					{
						BitData[Zim]-=Colors.Count;
						BitData[Zim+1]+=1; }
				}
			}
		}*/
		BitData = new int[PictureY, PictureX];
		nNumber = new ScottGarland.BigInteger (myProgress.ToString ());
		int N = Colors.Count;
		for (int Zim = PictureX * PictureY; Zim > 0; Zim--) {

			int iY = Mathf.FloorToInt ((Zim-1) / PictureX);
			int iX = Zim-iY*PictureX-1;

			BitData [iY,iX] = 0;

			ScottGarland.BigInteger result = new ScottGarland.BigInteger(1);

			for (ScottGarland.BigInteger I = new ScottGarland.BigInteger (Zim); !I.IsZero; I--) {
				result *= N;
			}
				
			int Gir = 1;
			while (nNumber > result * Gir) {
				Gir++;
			}
			BitData [iY,iX] = Gir-1;
			nNumber -= result * (Gir-1);
		}

		return true;
	}
	Texture2D DrawImageFromArray()
	{	
		Texture2D MyImage = new Texture2D (PictureX, PictureY);
		MyImage.filterMode = FilterMode.Point;

		for (int iY=0; iY<PictureY; iY++)
		{
			for (int iX=0; iX<PictureX; iX++)
			{
				MyImage.SetPixel(iX,iY,Colors[BitData[iY,iX]]);
			}
		}

		MyImage.Apply();
		return MyImage;
	}

	void UpdateTheImage()
	{
		ChangeMessage (3);
		myMessage += myProgress.ToString();
		Log ();

		Picture = DrawImageFromArray();
		/*	SaveImage ();
	}
	void SaveImage()
	{*/
		if (save) {
			byte[] bytes = Picture.EncodeToJPG ();

			System.IO.File.WriteAllBytes (DirectoryFolder + GetImageName (myProgress) + ".jpg", bytes);
		}
	}

	public ScottGarland.BigInteger GetLimit()
	{
		ScottGarland.BigInteger result = new ScottGarland.BigInteger(1);

		for (ScottGarland.BigInteger I = new ScottGarland.BigInteger (PictureX*PictureY); !I.IsZero ; I--) {
			result *= Colors.Count;
		}
		return result;
	}
}